class CreateSearchHistoryEntries < ActiveRecord::Migration
  def change
    create_table :search_history_entries do |t|
      t.references :user, index: true, foreign_key: true
      t.text :results_blob

      t.timestamps null: false
    end
  end
end
