# == Schema Information
#
# Table name: search_history_entries
#
#  id           :integer          not null, primary key
#  user_id      :integer
#  results_blob :text
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_search_history_entries_on_user_id  (user_id)
#

FactoryGirl.define do
  factory :search_history_entry do
    user nil
results_blob "MyText"
  end

end
