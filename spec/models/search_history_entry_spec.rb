# == Schema Information
#
# Table name: search_history_entries
#
#  id           :integer          not null, primary key
#  user_id      :integer
#  results_blob :text
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_search_history_entries_on_user_id  (user_id)
#

require 'rails_helper'

RSpec.describe SearchHistoryEntry, type: :model do

	describe '.add_user_search_results' do

		let(:user) { FactoryGirl.create :user }

		it "adds search results to user's history" do
			search_result = GenericSearchResult.new author: 'me', text: 'tomatos!'

			expect {
				SearchHistoryEntry.add_user_search_results(user, [search_result].to_json)
			}.to change(user, :recent_searches)
		end

	end

end
