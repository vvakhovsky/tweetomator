require 'rails_helper'

RSpec.describe TwitterSearchController, type: :controller do

	it "blocks unauthenticated access" do
		sign_in nil

		get :new
		expect(response).to redirect_to(new_user_session_path)

		get :search
		expect(response).to redirect_to(new_user_session_path)

		get :history
		expect(response).to redirect_to(new_user_session_path)
	end

  describe "GET #new" do
		before do
			sign_in
		end
		it "returns http success" do
      get :new
      expect(response).to have_http_status(:success)
		end

		it "renders the view" do
			get :new
			expect(response).to render_template :new
		end
  end

  describe "GET #search" do
		before do
			sign_in
		end

    it "has results if anything was found" do
			expect(TwitterClient).to receive(:recent_tweets) {
				[double(user: double(screen_name: 'me'), text: 'tomato juice something')]
			}

			post :search, { search: { term: 'tomato juice' }}

			assigns(:recent_tweets)
		end

		it "redirects back if user is not searching for TOMATO" do
			post :search, { search: { term: 'nothing' }}

			expect(response).to redirect_to twitter_search_new_path
		end
  end

end
