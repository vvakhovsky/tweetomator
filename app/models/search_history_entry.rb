# == Schema Information
#
# Table name: search_history_entries
#
#  id           :integer          not null, primary key
#  user_id      :integer
#  results_blob :text
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_search_history_entries_on_user_id  (user_id)
#

class SearchHistoryEntry < ActiveRecord::Base
  belongs_to :user

	def self.add_user_search_results(user, results_json)
		self.create! user: user, results_blob: results_json

	rescue => e
		Rails.logger.error "SearchHistoryEntry#add_user_search_results(#{user}, #{results_json}) failed. #{e.message}"
		false
	end
end
