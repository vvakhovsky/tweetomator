class TwitterClient
	MAX_RECENT_TWEETS = 10

	def self.recent_tweets(term)
		$twitter.search(term, result_type: "recent").take(MAX_RECENT_TWEETS)
	rescue => e
		Rails.logger.error "Failed fetching recent tweets: #{e.message}"
		nil
	end
end