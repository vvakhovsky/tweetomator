class GenericSearchResult

	attr_accessor :author, :text

	def initialize(attributes = {})
		self.author = attributes[:author] || ''
		self.text = attributes[:text] || ''
	end

	def self.parse_twitter(search_results)
		search_results.map do |r|
			self.new author: r.user.screen_name, text: r.text
		end
	end

	def self.load_from_json_text(json_blob)
		self.load_from_hash JSON.load(json_blob)
	end


	private

	def self.load_from_hash(search_results)
		search_results.map do |r|
			self.new author: r['author'.freeze], text: r['text'.freeze]
		end
	end
end