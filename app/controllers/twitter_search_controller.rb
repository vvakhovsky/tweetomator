class TwitterSearchController < ApplicationController
	before_action :authenticate_user!

  def new
  end

  def search

    unless safe_search_params['term']['tomato']
      redirect_to twitter_search_new_path, notice: 'Search term should include \'tomato\''
      return
    end

    @recent_tweets = TwitterClient.recent_tweets safe_search_params['term']

    if @recent_tweets
			SearchHistoryEntry.add_user_search_results current_user, GenericSearchResult.parse_twitter(@recent_tweets).to_json
    end
  end

  def history

		@search_history = current_user.recent_searches

  end

  private

  def safe_search_params
    params.require(:search).permit(:term)
  end
end
